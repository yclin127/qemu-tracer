#!/usr/bin/python

import math
import random
import sys

__size = lambda b: 1<<(b) if b > 0 else 0
__trim = lambda x,b,o: (((x)>>(o))&(__size(b)-1))
__mask = lambda x,b,o: ((x)&((__size(b)-1)<<(o)))

CACHE_LINE_BITS = 6
CACHE_SET_BITS  = 14
CACHE_WAY_COUNT = 8

import numpy as np
import matplotlib.pyplot as plt

def plot_begin():
    plt.ion()
    plt.clf()
    plt.xscale('log')
    plt.yscale('log')
    
def plot_end():
    plt.ioff()
    
def plot_data(x, y, color, marker='.'):
    plt.plot(x, y, marker=marker, ls='', color=color, alpha=0.1)
    plt.draw()

class Histogram:
    
    def __init__(self, lower, upper, steps=64):
        self.lower = math.log(lower)
        self.upper = math.log(upper)
        self.steps = steps
        self.ratio = (self.upper-self.lower)/(self.steps-1)
        self.bins = [
            math.exp(self.lower+self.ratio*i)
        for i in range(self.steps)]
        
    def reset(self):
        self.num = [0. for i in range(self.steps)]
        self.sum = [0. for i in range(self.steps)]
        self.zero = 0
        
    def count(self, x):
        if x == 0:
            self.zero += 1
            return
        l = int((math.log(x)-self.lower)/self.ratio)
        self.num[l] += 1
        self.sum[l] += x
        

class Statistic:
    
    def __init__(self, domains, groups, limits=(1,1<<32)):
        self.last_touch = None
        self.hist = Histogram(limits[0], limits[1])
        self.domains = domains
        self.groups = groups
        
    def reset(self, icount):
        self.last_touch = [[icount
            for j in range(self.groups)]
            for i in range(self.domains)]
        self.hist.reset()
        
    def count(self, domain, group, icount):
        last_icount = self.last_touch[domain][group]
        self.last_touch[domain][group] = icount
        self.hist.count(icount - last_icount)
    
EPOCH  = 24
DOMAIN = (4,13)
GROUP  = (6,None)
PLACE  = (15,17)

last_epoch = None
stat1 = Statistic(__size(DOMAIN[0]), __size(GROUP[0]), (1,1<<EPOCH))
stat2 = Statistic(__size(DOMAIN[0]), __size(GROUP[0]), (1,1<<EPOCH))

def trace_file_begin():
    global last_epoch, stat1, stat2
    last_epoch = 0
    stat1.reset(last_epoch)
    stat2.reset(last_epoch)
    plot_begin()

def trace_file_end():
    global last_epoch
    last_epoch = None
    plot_end()

def trace_file_log(vaddr, paddr, flags, icount):
    global last_epoch, stat1, stat2
    
    epoch = __mask(icount, -1, EPOCH)
    while last_epoch != epoch:
        last_epoch += __size(EPOCH)
        for domain in range(__size(DOMAIN[0])):
            for group in range(__size(GROUP[0])):
                stat1.count(domain, group, last_epoch)
                stat2.count(domain, group, last_epoch)
        plot_data(stat1.hist.bins, stat1.hist.num, 'r', '.')
        plot_data(stat2.hist.bins, stat2.hist.num, 'b', '.')
        plot_data(stat1.hist.bins, stat1.hist.sum, 'r', '+')
        plot_data(stat2.hist.bins, stat2.hist.sum, 'b', '+')
        stat1.reset(last_epoch)
        stat2.reset(last_epoch)
    
    domain = __trim(paddr, DOMAIN[0], DOMAIN[1])
    place  = __trim(paddr, PLACE[0], PLACE[1])
    
    group1 = __trim(place, GROUP[0], 0)
    stat1.count(domain, group1, icount)
    
    group2 = __trim(place, GROUP[0], PLACE[0]-GROUP[0])
    stat2.count(domain, group2, icount)
